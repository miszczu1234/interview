<%@ tag language="java" pageEncoding="utf-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/templates"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@attribute name="body" fragment="true"%>
<%@attribute name="title" fragment="true"%>
<%@attribute name="closeBtnLabel" fragment="true" %>

<div id="loginModal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4><jsp:invoke fragment="title" /></h4>
			</div>
			<div class="modal-body">
				<jsp:invoke fragment="body" />
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal"><jsp:invoke fragment="closeBtnLabel" /></button>
			</div>
		</div>
	</div>
</div>