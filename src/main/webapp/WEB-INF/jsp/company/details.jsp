<%@ page language="java" contentType="text/html; utf-8"
         pageEncoding="utf-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/templates"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/templates"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<jsp:useBean id="commentBean" scope="request" class="com.itinterview.bean.CommentBean" />

<spring:url value="/company/comment/edit" var="editCompanyCommentUrl" />

<t:pagetemplate>
    <jsp:attribute name="content">
        <div class="panel panel-default">
            <div class="panel-body">
                <h2><strong><c:out value="${company.name}" /></strong></h2>
                <sec:authorize access="isAuthenticated()">
                <div class="button-op-panel">
                	<spring:url value="/company/edit/{companyId}" var="editCompanyLink">
						<spring:param name="companyId" value="${company.id}" />
					</spring:url>
                    <a class="btn btn-primary" href="${fn:escapeXml(editCompanyLink)}">Edytuj Dane Firmy</a>
                    <sec:authorize access="hasRole('ROLE_ADMIN')">
                    	<a class="btn btn-primary">Usuń</a>
                    </sec:authorize>
                </div>
                </sec:authorize>
                <form>
                    <div class="form-group">
                        <label  for="companyName"><strong>Nazwa</strong></label>
                        <div><c:out value="${company.name}"/></div>
                    </div>
                    <div class="form-group">
                        <label  for="companyWeb">Strona internetowa</label>
                        <div><a href="${company.website}"><c:out value="${company.website}"/></a></div>
                    </div>
                    <div class="form-group">
                        <label  for="companyTechs">Używane Technologie</label>
                        <div class="tag-container">
                        	<c:forEach var="companyTechnologies" items="${company.companyTechnolgieses}">
                        		<label class="tag">
                        			<c:out value="${companyTechnologies.technology.value}" />
                        		</label>
                        	</c:forEach>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="companyDesc">Opis i co warto wiedzieć</label>
                        <div><c:out value="${company.description}" /></div>
                    </div>
                    <a class="btn btn-primary" href="${pageContext.request.contextPath}/question/list/${company.id}">Pytania rekrutacyjne</a>
                </form>
            </div>
        </div>
            <div class="chat-box">
            	<h2>Komentarze</h2>
                <c:forEach var="comment" items="${comments}">
                	<div class="chat-message">
                    <div class="chat-message-info">
                        <label class="chat-message-date"><c:out value="${comment.createddate}"/></label>
                        <label class="chat-message-author"><c:out value="${comment.user.login}"/></label>
                        <sec:authorize access="isAuthenticated()">
                        	<c:if test="${sessionScope.SPRING_SECURITY_CONTEXT.authentication.principal.username.equals(comment.user.login)}">
                        		<form class="edit-comment-form" action="${editCompanyCommentUrl}" method="post">
                        			<input type="hidden" value="${comment.id}" name="commentId" />
                        			<input type="hidden" value="${company.id}" name="companyId" />
                        			<input class="btn-link edit-link" type="submit" value="Edytuj Post" />
                        		</form>
                        	</c:if>
                        </sec:authorize>
                    </div>
                    <div class="chat-message-text">
                        <c:out value="${comment.text}"/>
                    </div>
               	 	</div>
                </c:forEach>
                <sec:authorize access="isAuthenticated()">
                <div class="chat-box">
                	<c:if test="${edit==true}">
                		<spring:url value="/company/comment/edit/save/${company.id}" var="actionLink"/>
                	</c:if>
                	<c:if test="${edit!=true}">
                		<spring:url value="/company/comment/add/${company.id}" var="actionLink"/>
                	</c:if>
                	<t:validatorAction />
                    <form:form method="post" role="form" commandName="commentBean" action="${actionLink}">
                        <form:textarea path="text" class="form-control" id="companyDesc" rows="5"></form:textarea>
                        <form:hidden path="commentId" />
                        <input class="btn btn-primary" type="submit" value="Opublikuj Komentarz"/>
                    </form:form>
                </div>
                </sec:authorize>
                <sec:authorize access="hasRole('ROLE_ANONYMOUS')">
                	<div class="alert alert-info"><strong>Uwaga!</strong> Musisz być zalogowany aby dodać komentarz.</div>
                </sec:authorize>
            </div>
    </jsp:attribute>
</t:pagetemplate>