<%@ page language="java" contentType="text/html; utf-8"
	pageEncoding="utf-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/templates"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<jsp:useBean id="companyBean" scope="request" class="com.itinterview.bean.CompanyBean" />

<head>
<script src="<c:url value="/resources/js/tag/tags.js" />"></script>
</head>

<t:pagetemplate>
	<jsp:attribute name="content">
        <div class="search-title-container">
            <div id="add-company" class="search-title">Dodaj Firme</div>
        </div>
         <c:if test="${edit!=null}">
        	<c:set var="actionTarget" value="${pageContext.request.contextPath}/company/edit" />
        </c:if>
        <c:if test="${edit==null}">
        `	<c:set var="actionTarget" value="${pageContext.request.contextPath}/company/add" />
        </c:if>
        <t:validatorAction />
        <form:form method="post" role="form" commandName="companyBean" action="${actionTarget}">
            <div class="form-group">
                <label for="companyName">Nazwa Firmy</label>
                <form:input path="name" type="text" class="form-control" id="companyName" />
                <div><label class="alert-danger"><form:errors path="name"/></label></div>
            </div>
            <div class="form-group">
                <label for="companyWeb">Oficjalna Strona Internetowa Firmy</label>
                <form:input path="website" type="text" class="form-control" id="companyWeb" />
            </div>
            <div class="form-group">
                <label for="companyTechs">Używane Technologie</label><br />
                <input type="text" class="form-control"
					id="companyTechs" />
                <a class="btn btn-default" href=""
					onclick="addTagFromInput();return false;">Dodaj</a>
                <div id="tagsContainer" class="tag-container">
                    
                </div>
                <form:input  path="technologies" id="techsType" type="hidden"></form:input>
            </div>
            <div class="form-group">
                <label for="companyDesc">Opis i co warto wiedzieć</label>
                <form:textarea path="description" class="form-control" id="companyDesc" rows="5"></form:textarea>
                <div><label class="alert-danger"><form:errors path="description"/></label></div>
            </div>
            <div class="submit-btn-container">
                <button type="submit" class="btn btn-default">
					<strong>Zapisz</strong>
				</button>
            </div>
            <c:if test="${edit!=null}">
            	<form:input path="id" value="${companyBean.id}" type="hidden" />
            </c:if>
            </form:form>
            </jsp:attribute>
</t:pagetemplate>