<%@ page language="java" contentType="text/html; utf-8"
         pageEncoding="utf-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/templates"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<script src="<c:url value="/resources/js/text/text.js" />"></script>
 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<t:pagetemplate>
    <jsp:attribute name="content">
        <div class="search-title-container">
            <div id="company_title" class="search-title">Ostatnio dodane pytania</div>
        </div>
        <div>
            <ul id="id-last-question-list" class="list-group">
                <c:forEach var="question" items="${questions}">
        			<li class="list-group-item">
        				<div class="company-title">
        					<c:forEach items="${question.questionCompanies}" var="questionCompany">
        						<spring:url value="/company/details/${questionCompany.company.id}" var="companyLink"/>
        						<a href="${companyLink}"><c:out value="${questionCompany.company.name}"/></a>
        						<br />
        					</c:forEach>
        				</div>
        				<div class="question-list-text">
        					<!--<c:set var="text" value="${question.text}"/>
        					<c:set var="id_question_element" value="id_question_${question.id}" />
        					<p id="<c:out value="${id_question_element}" />"><script>cut('${text}', '${id_question_element}')</script></p>-->
        					<p><c:out value="${question.text}" /></p>
        					<spring:url value="/question/details/{questionId}" var="questionShow">
        						<spring:param name="questionId" value="${question.id}"/>
        					</spring:url>
        					<a href="${fn:escapeXml(questionShow)}">Czytaj dalej</a>
        				</div>
        			</li>	
        		</c:forEach>
            </ul>
        </div>
    </jsp:attribute>
</t:pagetemplate>