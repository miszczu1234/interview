/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itinterview.security;

import java.util.ArrayList;
import java.util.Collection;

import javax.naming.AuthenticationException;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.itinterview.entity.User;
import com.itinterview.entity.UserType;
import com.itinterview.entity.enums.UserStatus;

/**
 *
 * @author kurekk
 */
public class SecurityUser implements UserDetails{

	private static final long serialVersionUID = 1L;

	private GrantedAuthorityFactory grantedAuthorityFactory;
	
	private User user;
	
	public SecurityUser(User user) {
		this.user= user;
		grantedAuthorityFactory = new GrantedAuthorityFactory();
	}
	
	public Collection<? extends GrantedAuthority> getAuthorities() {
		
		return grantedAuthorityFactory.getAuthorities(user);
	}

	public String getPassword() {
		return user.getPassword();
	}

	public String getUsername() {
		return user.getLogin();
	}

	public boolean isAccountNonExpired() {
		return true;
	}

	public boolean isAccountNonLocked() {
		return true;
	}

	public boolean isCredentialsNonExpired() {
		return true;
	}

	public boolean isEnabled(){
		
		switch(UserStatus.getByValue(user.getStatus())){
		case ACTIVATED:
			return true;
		case DISACTIVATED:
		case NOT_ACTIVATED:
		default:
			return false;
		}
	
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
}
