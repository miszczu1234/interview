package com.itinterview.comparator;

import java.util.Comparator;
import java.util.Date;

import com.itinterview.entity.Question;

public class QuestionComparatorByDate implements Comparator<Question>{

	@Override
	public int compare(Question o1, Question o2) {
		
		Date date1= o1.getDate();
		Date date2= o2.getDate();
	
		if((date1!=null) && (date2!=null)){
			return date1.compareTo(date2);
		}
		if((date1==null) && (date2!=null)){
			return -1;
		}
		if((date1!=null) && (date2==null)){
			return 1;
		}
		
		return 0;
	}
	
}
