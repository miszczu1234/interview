/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itinterview.bean;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

/**
 *
 * @author kurekk
 */

@Component
@Scope(value="request", proxyMode=ScopedProxyMode.TARGET_CLASS)
public class RegisterUserBean extends UserBean{
    @NotNull
    @Size(min=1, message="Input the password")
    private String passRep;

    @NotNull
    @Size(min=1, message="Incorrect e-mail")
    private String repEmail;

    /**
     * @return the repEmail
     */
    public String getRepEmail() {
        return repEmail;
    }

    /**
     * @param repEmail the repEmail to set
     */
    public void setRepEmail(String repEmail) {
        this.repEmail = repEmail;
    }

    /**
     * @return the passRep
     */
    public String getPassRep() {
        return passRep;
    }

    /**
     * @param passRep the passRep to set
     */
    public void setPassRep(String passRep) {
        this.passRep = passRep;
    }
}
