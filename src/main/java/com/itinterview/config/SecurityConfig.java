package com.itinterview.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.Authentication;

import com.itinterview.entity.UserType;
import com.itinterview.entity.enums.UserTypes;
import com.itinterview.service.SecurityUserDetailsService;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter{
	
	private static final String PERMIT_ALL[] = new String[]{"/", "/user/login", "/user/login**", "/user/register", "/user/logout"};
	private static final String ROLE_USER[] = new String[]{"/company/add"};
	private static final String ROLE_ADMIN[]= new String[]{"/user/list"};
	
	private static final String IGNORING[] = new String[]{"/resources/**"};
	
	private static final String LOGIN_PAGE = "/user/login";
	private static final String LOGIN_PROCESSING = "/user/login";
	private static final String FAILURE_URL = "/user/login";
	private static final String SUCC_URL = "/user/login";
	private static final String LOGOUT_URL = "/user/logout";
	private static final String LOGOUT_SUCC_URL = "/"; 
	
	@Autowired
	private SecurityUserDetailsService userDetailsService;
	
	protected void configure(AuthenticationManagerBuilder registry) throws Exception{
		registry.userDetailsService(userDetailsService);
	}
	
	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers(IGNORING);
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.csrf().disable().authorizeRequests()
			.antMatchers(PERMIT_ALL).permitAll()
			.antMatchers(ROLE_USER).hasRole("USER")
			.antMatchers(ROLE_ADMIN).hasRole("ADMIN")
		.and()
			.formLogin()
			.loginPage(LOGIN_PAGE)
			.loginProcessingUrl(LOGIN_PROCESSING)
			.defaultSuccessUrl(SUCC_URL)
			.failureUrl(FAILURE_URL)
			.permitAll()
		.and()
			.logout()
			.logoutUrl(LOGOUT_URL)
			.logoutSuccessUrl(LOGOUT_SUCC_URL);
	}
}
