package com.itinterview.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itinterview.dao.CompanyTechnologiesDao;
import com.itinterview.entity.Company;
import com.itinterview.entity.CompanyTechnolgies;
import com.itinterview.entity.Technology;

@Service
public class CompanyTechnologiesService {
	
	@Autowired
	private CompanyTechnologiesDao companyTechDao;
	
	public List<CompanyTechnolgies> getByCompanyAndTechnology(String companyName, String technologyValue){
		return companyTechDao.findByCompanyNameAndTechnologyValue(companyName, technologyValue);
	}
	
	public CompanyTechnolgies persist(Company company, Technology technology){
		
		List<CompanyTechnolgies> companyTechs= 
				companyTechDao.findByCompanyNameAndTechnologyValue(
						company.getName(), technology.getValue());
		
		if(!companyTechs.isEmpty()){
			companyTechs.get(0);
		}
		
		CompanyTechnolgies companyTech= new CompanyTechnolgies();
		companyTech.setCompany(company);
		companyTech.setTechnology(technology);
		companyTech= companyTechDao.save(companyTech);
		
		return companyTech;
	}
	
	public void delete(Integer id){
		companyTechDao.deleteById(id);
	}
}
