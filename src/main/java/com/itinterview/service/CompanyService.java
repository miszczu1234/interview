package com.itinterview.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itinterview.bean.CompanyBean;
import com.itinterview.comparator.CompanyComparatorByDate;
import com.itinterview.dao.CompanyDao;
import com.itinterview.entity.Company;
import com.itinterview.entity.CompanyTechnolgies;
import com.itinterview.entity.Question;
import com.itinterview.entity.QuestionCompany;
import com.itinterview.entity.Technology;
import com.itinterview.entity.factory.EntityModelFactory;

@Service
public class CompanyService {
	
	private static final Logger LOG= Logger.getLogger(CompanyService.class);
	
	@Autowired 
	private EntityModelFactory entityModelFactory;
	@Autowired 
	private CompanyDao companyDao;
	@Autowired
	private TechnologyService techService;
	@Autowired
	private CompanyTechnologiesService companyTechService;
	
	public List<Company> getAllComapny(){
		return companyDao.findAllByOrderByNameAsc();
	}
	
	public List<Company> getCompaniesNameStartsWith(String startWith){
		String nameParam= startWith+"%";
		return companyDao.searchWithJPQLQuery(nameParam);
	}
	
	public Company persist(CompanyBean companyBean){
		Company company= entityModelFactory.createCompany(companyBean);
		company= companyDao.save(company);
		
		List<String> techs= companyBean.getTechnolgiesAsList();
		List<Technology> persistedTechnology= techService.persist(techs);
		
		for(Technology tech : persistedTechnology){
			companyTechService.persist(company, tech);
		}
		
		return company;
	}
	
	private List<CompanyTechnolgies> removeOldTechnologies(Collection<CompanyTechnolgies> companyTechs, List<String> technologies){
		List<CompanyTechnolgies> oldTechs= new ArrayList<>();
		Iterator<CompanyTechnolgies> companyTechsIt= companyTechs.iterator();
		
		while(companyTechsIt.hasNext()){
			CompanyTechnolgies companyTech= companyTechsIt.next();
			if(technologies.contains(companyTech.getTechnology().getValue())){
				technologies.remove(companyTech.getTechnology().getValue());
			}else{
				oldTechs.add(companyTech);
			}
//			else{
//				companyTechs.remove(companyTech);
//				LOG.debug(String.format("removeOldTechnologies : removed technology %s", companyTech.getTechnology().getValue()));
//			}
		}
		
		return oldTechs;
	}
	
	private List<CompanyTechnolgies> updateTechnologies(Company company, List<String> technologies){
		Set<CompanyTechnolgies> companyTechs= company.getCompanyTechnolgieses();
		List<CompanyTechnolgies> oldTechs= removeOldTechnologies(companyTechs, technologies);
		for(String tech : technologies){
			Technology newTechnology= techService.getByName(tech);
			if(newTechnology==null){
				newTechnology= entityModelFactory.createTechnology(tech);
			}
			LOG.debug(String.format("updateTechnologies : new technology %s", newTechnology.getValue()));
			CompanyTechnolgies compTech= entityModelFactory.createCompanyTechnology(company, newTechnology);
			companyTechs.add(compTech);
		}
		
		return oldTechs;
	}
	
	private void deleteOldTechs(Collection<CompanyTechnolgies> compTechs){
		for(CompanyTechnolgies compTech : compTechs){
			companyTechService.delete(compTech.getId());
			LOG.debug(String.format("deleteOldTechs : technology %d removed", compTech.getId()));
		}
	}
	
	public Company update(CompanyBean companyBean){
		Company companyToUpdate= companyDao.findOne(companyBean.getId());
		if(companyToUpdate==null){
			LOG.debug(String.format("update : company [id:%d] doesn't exist", companyBean.getId()));
			return companyToUpdate;
		}
		companyToUpdate= entityModelFactory.updateCompany(companyToUpdate, companyBean);
		List<CompanyTechnolgies> oldTechs= updateTechnologies(companyToUpdate, companyBean.getTechnolgiesAsList());
		LOG.debug(String.format("update : technologies to persist %d", companyToUpdate.getCompanyTechnolgieses().size()));
		companyDao.save(companyToUpdate);
		LOG.debug(String.format("update : technologies to delete %d", oldTechs.size()));
		deleteOldTechs(oldTechs);
		return companyToUpdate;
	}
	
	public Company getById(Integer id){
		return companyDao.findOne(id);
	}
	
	public List<Question> getQuestionsForCompany(Integer id){
		
		List<Question> questions= new ArrayList<Question>();
		
		Company company = getById(id);
		Set<QuestionCompany> questionCompanies= company.getQuestionCompanies();
		for(QuestionCompany questionCompany : questionCompanies){
			Question question = questionCompany.getQuestion();
			if(question==null){
				continue;
			}
			questions.add(question);
		}
		
		return questions;
	}
	
	public List<Company> getLast(int count){
		List<Company> companies= companyDao.findAll();
		Collections.sort(companies, new CompanyComparatorByDate());
		
		if(companies.size() > count){
			return companies.subList(0, count);
		}
		
		return companies;
	}
	
	public Company getByName(String name){
		return companyDao.findOneByName(name);
	}
	
	public List<Company> getContainingName(String name){
		return companyDao.findByNameContainingIgnoreCase(name);
	}
}
