package com.itinterview.service;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itinterview.bean.AnswerBean;
import com.itinterview.dao.AnswerDao;
import com.itinterview.entity.Answer;
import com.itinterview.entity.Question;
import com.itinterview.entity.QuestionAnswer;
import com.itinterview.entity.factory.EntityModelFactory;

@Service
public class AnswerService {
	
	@Autowired
	private AnswerDao answerDao;
	
	@Autowired 
	private EntityModelFactory entityModelFactory;
	
	@Autowired
	private QuestionService questionService;
	
	public Answer persist(Answer answer){
		return answerDao.save(answer);
	}
	
	public Answer persist(AnswerBean answerBean){
		Answer answer= entityModelFactory.createAnswer(answerBean);
		Question question= questionService.getById(answerBean.getQuestionId());
		QuestionAnswer questionAnswer= entityModelFactory.createQuestionAnswer(question, answer);
		Set<QuestionAnswer> questionAnswers= new HashSet<>();
		questionAnswers.add(questionAnswer);
		answer.setQuestionAnswers(questionAnswers);
		answer= answerDao.save(answer);
		
		return answer;
	}
	
	public Answer getById(Integer id){
		Answer answer= answerDao.findOne(id);
		
		return answer;
	}
	
	public Answer update(AnswerBean answerBean){
		Answer answerToEdit= answerDao.getOne(answerBean.getAnswerId());
		answerToEdit.setText(answerBean.getText());
		answerToEdit= answerDao.save(answerToEdit);
		
		return answerToEdit;
	}
}
