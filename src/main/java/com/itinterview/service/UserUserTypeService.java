package com.itinterview.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itinterview.dao.UserUserTypeDao;
import com.itinterview.entity.UserUserType;

@Service
public class UserUserTypeService {
	
	@Autowired
	private UserUserTypeDao userUserTypeDao;
	
	public UserUserType persist(UserUserType userUserType){
		
		userUserType= userUserTypeDao.save(userUserType);
		
		return userUserType;
	}
}
