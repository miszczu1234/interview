/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itinterview.dao;

import com.itinterview.entity.UserType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.Repository;

/**
 *
 * @author kurekk
 */
public interface UserTypeDao extends JpaRepository<UserType, Integer>{
    public UserType findOneById(Long id);
    public UserType findOneByType(String type);
}
