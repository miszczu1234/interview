package com.itinterview.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.itinterview.entity.UserUserType;

public interface UserUserTypeDao extends JpaRepository<UserUserType, Integer>{
	
}
