package com.itinterview.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.itinterview.entity.Answer;

public interface AnswerDao extends JpaRepository<Answer, Integer>{

}
