package com.itinterview.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

import com.itinterview.bean.CompanyBean;
import com.itinterview.service.CompanyService;
import com.itinterview.validator.core.Result;
import com.itinterview.validator.core.ValidationResult;
import com.itinterview.validator.core.Validator;

@Component
@Scope(value="session", proxyMode=ScopedProxyMode.TARGET_CLASS)
public class CompanyValidator extends Validator<CompanyBean>{

	private static final String COMPANY_EXIST = "Firma o danej nazwie juz istnieje";
	
	@Autowired
	private CompanyService companyService;
	
	public CompanyValidator() {
		super();
	}
	
	private boolean checkIfNameExist(){
		return (companyService.getByName(objectToValidate.getName())==null) ? false : true;
	}
	
	protected Result performValidation(){
		Result result= new Result(ValidationResult.VALID);
		
		if(checkIfNameExist()){
			result.addMessage(COMPANY_EXIST);
			result.setResult(ValidationResult.NOT_VALID);
		}
		
		return result;
	}

}
