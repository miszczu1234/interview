package com.itinterview.validator.core;

public enum ValidationResult {
	VALID,
	NOT_VALID;
}
