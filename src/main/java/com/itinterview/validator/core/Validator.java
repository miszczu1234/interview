package com.itinterview.validator.core;

public abstract class Validator<T> {

	protected T objectToValidate;
	protected Result result;
	
	public Validator(){
		result= new Result(ValidationResult.NOT_VALID);
	}
	
	protected abstract Result performValidation();
	
	public Result isValid(T objectToValidate){
		this.objectToValidate= objectToValidate;
		result= performValidation();
		
		return result;
	}
	
	public Result getResult() {
		return result;
	}
	
	public T getObjectToValidate(){
		return objectToValidate;
	}
}
