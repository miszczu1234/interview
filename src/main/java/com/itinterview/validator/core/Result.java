package com.itinterview.validator.core;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;

public class Result {
	
	private static final Logger LOG= Logger.getLogger(Result.class);
	
	private ValidationResult result;
	private List<String> messages;
	
	private Result(){
		messages= new LinkedList<>();
	}
	
	public Result(ValidationResult result) {
		this();
		this.result= result;
	}

	public ValidationResult setResult(ValidationResult result){
		ValidationResult prevResult= this.result;
		this.result= result;
		
		return prevResult;
	}

	public ValidationResult getResult() {
		return result;
	}
	
	public List<String> getMessages() {
		List<String> messages= new LinkedList<>(this.messages);
		return messages;
	}

	public void addMessage(String message){
		messages.add(message);
	}
	
	public String getMessageAsString(){
		LOG.debug(String.format("getMessageAsString : %s", Arrays.toString(messages.toArray())));
		String message= Arrays.toString(messages.toArray());
		message= message.replaceAll("[\\[\\]]", "");
		
		return message;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this.result == null) ? 0 : this.result.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Result other = (Result) obj;
		if (result != other.result)
			return false;
		return true;
	}
}
