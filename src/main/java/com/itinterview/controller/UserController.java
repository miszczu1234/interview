/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itinterview.controller;

import com.itinterview.bean.RegisterUserBean;
import com.itinterview.bean.UserBean;
import com.itinterview.controller.enums.HtmlAttributes;
import com.itinterview.dao.UserDao;
import com.itinterview.entity.User;
import com.itinterview.entity.enums.UserTypes;
import com.itinterview.service.UserService;
import com.itinterview.validator.UserValidator;
import com.itinterview.validator.core.Result;
import com.itinterview.validator.core.ValidationResult;
import com.itinterview.validator.core.Validator;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author kurekk
 */

@Controller
@RequestMapping("/user")
public class UserController {
    
	private static final Logger LOG= Logger.getLogger(UserController.class);
	
    @Autowired
    private RegisterUserBean registerUserBean;
    
    @Autowired
    @Qualifier("userService")
    private UserService userService;
    
    @Autowired
    private UserValidator userValidator;
    
    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public String registerGet(){
        return "/user/register";
    }
    
    @RequestMapping(value = "/register", method= RequestMethod.POST)
    public String registerPost(@ModelAttribute("registerUserBean")@Validated RegisterUserBean registerUserBean,
            BindingResult bindingResult, HttpServletRequest req){
        
        if(bindingResult.hasErrors()){
            return "user/register";
        }
        
        Result result= userValidator.isValid(registerUserBean);
        if(result.getResult().equals(ValidationResult.NOT_VALID)){
        	req.setAttribute(HtmlAttributes.MSG_ERR.getAttribute(), result.getMessageAsString());
        	return "user/register";
        }
        
        userService.persistUser(registerUserBean, UserTypes.BASIC);
        req.setAttribute(HtmlAttributes.MSG_SUCC.getAttribute(), "Uzytkownik zarejestrowany");
        
        return "user/register";
    }
    
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String loginGet(HttpServletRequest req, RedirectAttributes redirectAttributes){
    	
    	String referer = req.getHeader("Referer");
    	redirectAttributes.addFlashAttribute("loginModal", true);
    	redirectAttributes.addFlashAttribute("login_error", true);
    	
        return "redirect:"+referer;
    }

    @RequestMapping(value="/login", method= RequestMethod.POST)
    public String loginPost(HttpServletRequest req){

    	String referer = req.getHeader("Referer");
    	LOG.debug(String.format("loginPost : referer '%s'", referer));
    	
        return "redirect:"+ referer;
    }
    
    @RequestMapping(value="/logout", method= RequestMethod.GET)
    public String logoutGet(){
    	return "/index";
    }
    
    @RequestMapping(value="/list", method= RequestMethod.GET)
    public String listUsersGet(HttpServletRequest req){
    	
    	List<User> users= userService.getAll();
    	req.setAttribute("users", users);
    	
    	return "user/list";
    }
    
    @RequestMapping(value="/edit", method=RequestMethod.POST)
    public String userEditPost(@RequestParam(required=false) Integer userId,
    		@RequestParam(required=false) Integer userStatus, HttpServletRequest req,
    		RedirectAttributes redirectAttrs){

    	userService.update(userId, userStatus);
    	redirectAttrs.addFlashAttribute(HtmlAttributes.MSG_SUCC.getAttribute(), "Konto uzytkownika zostalo zaktualizowane");
    	
    	return "redirect:/user/list";
    }
}
