/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itinterview.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.itinterview.bean.AnswerBean;
import com.itinterview.bean.CommentBean;
import com.itinterview.bean.QuestionBean;
import com.itinterview.bean.factory.FactoryBean;
import com.itinterview.comparator.QuestionAnswerComparatorByAnswerDate;
import com.itinterview.comparator.QuestionCommentComparatorByDate;
import com.itinterview.comparator.QuestionComparatorByDate;
import com.itinterview.controller.enums.HtmlAttributes;
import com.itinterview.entity.Answer;
import com.itinterview.entity.Comment;
import com.itinterview.entity.Company;
import com.itinterview.entity.Question;
import com.itinterview.entity.QuestionAnswer;
import com.itinterview.entity.QuestionComment;
import com.itinterview.security.SecurityManager;
import com.itinterview.security.SecurityUser;
import com.itinterview.service.AnswerService;
import com.itinterview.service.CommentService;
import com.itinterview.service.CompanyService;
import com.itinterview.service.QuestionService;
import com.itinterview.sort.QuestionSorter;

/**
 *
 * @author kurekk
 */

@Controller
@RequestMapping("question")
public class QuestionController {
    
	private static final Logger LOG= Logger.getLogger(QuestionController.class);
	
	@Autowired
	private QuestionBean questionBean;
	@Autowired
	private QuestionService questionService;
	@Autowired
	private CommentService commentService;
	@Autowired 
	private SecurityManager securityManager;
	@Autowired
	private CompanyService companyService;
	@Autowired
	private CommentBean commentBean;
	@Autowired
	private FactoryBean factoryBean;
	@Autowired
	private AnswerService answerService;
	
    @RequestMapping(value = "/details/{id}", method = RequestMethod.GET)
    public String detailsGet(HttpServletRequest req, @PathVariable Integer id){
    	
    	Question question = questionService.getById(id);
    	QuestionSorter.sortByQuestionAnswerCreateDate(question);
    	req.setAttribute("question", question);	
    	
    	Set<QuestionComment> questionComments= question.getQuestionComments();
    	Collections.sort(new LinkedList<QuestionComment>(questionComments), new QuestionCommentComparatorByDate());
    	req.setAttribute("questionComments", questionComments);
    	
        return "question/details"; 
    }
    
    @RequestMapping(value="comment/{id}", method = RequestMethod.GET)
    public String commentGet(@PathVariable Integer id, HttpServletRequest req){
    	
    	Question question= questionService.getById(id);
    	QuestionSorter.sortByQuestionAnswerCreateDate(question);
    	req.setAttribute("question", question);
    	
    	List<Comment> comments= commentService.getCommentsForQuestion(id);
    	req.setAttribute("comments", comments);
    	
        return "question/comment";
    }
    
    @RequestMapping(value="/list/{id}", method= RequestMethod.GET)
    public String questionGet(HttpServletRequest req, @PathVariable Integer id){
    	
    	Company company= companyService.getById(id);
    	req.setAttribute("company", company);
    	List<Question> questions =  companyService.getQuestionsForCompany(id);
    	Collections.sort(questions, new QuestionComparatorByDate());
    	for(Question question: questions){
    		QuestionSorter.sortByQuestionAnswerCreateDate(question);
    	}
    	req.setAttribute("questions", questions);
    	
    	return "question/questions";
    }
    
    @RequestMapping(value="/add/{id}", method = RequestMethod.GET) 
    private String addQuestionGet(HttpServletRequest req, @PathVariable Integer id){
    	
    	req.setAttribute(HtmlAttributes.ATTR_ID_CPMANY.getAttribute(), id);
    	
    	return "question/add";
    }
    
    @RequestMapping(value="/add/{id}", method = RequestMethod.POST) 
    private String addQuestionPost(@ModelAttribute("questionBean")@Validated QuestionBean questionBean,
    		BindingResult bindingResult, @PathVariable Integer id, HttpServletRequest req){
    	
    	if(bindingResult.hasErrors()){
    		req.setAttribute(HtmlAttributes.MSG_ERR.getAttribute(), "Blad w formularzu");
    		return "question/add";
    	}
    	
    	SecurityUser user= securityManager.getCurrentUser();
    	questionBean.setAuthor(user.getUser());
    	questionBean.setCompanyId(id);
    	questionService.persist(questionBean);
    	
    	req.setAttribute(HtmlAttributes.MSG_SUCC.getAttribute(), "Dodano nowe pytanie");
    	
    	return "question/add";
    }
    
    @RequestMapping(method=RequestMethod.POST, value="/comment/add/{id}")
    public String addCommentPost(@ModelAttribute("commentBean")@Validated CommentBean commentBean,
    		BindingResult bindingResult, @PathVariable Integer id){
    	
    	if(bindingResult.hasErrors()){
    		return "redirect:/question/comment/"+id;
    	}
    	
    	commentBean.setAuthor(securityManager.getCurrentUser().getUser());
    	commentBean.setDate(new Date());
    	commentService.persistCommentQuestion(commentBean, id);
    	
    	return "redirect:/question/comment/"+id;
    }
}
