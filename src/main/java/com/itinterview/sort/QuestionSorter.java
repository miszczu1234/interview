package com.itinterview.sort;

import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Component;

import com.itinterview.comparator.QuestionAnswerComparatorByAnswerDate;
import com.itinterview.entity.Question;
import com.itinterview.entity.QuestionAnswer;

@Component
public class QuestionSorter {
	public static void sortByQuestionAnswerCreateDate(Question question){
    	List<QuestionAnswer> questionAnswers= new LinkedList<>(question.getQuestionAnswers());
    	Collections.sort(questionAnswers, new QuestionAnswerComparatorByAnswerDate());
		Set<QuestionAnswer> questionAnswerSorted= new LinkedHashSet<>(questionAnswers);
    	question.setQuestionAnswers(questionAnswerSorted);
	}
}
